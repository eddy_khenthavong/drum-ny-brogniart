$( document ).ready(function() {

    //  Initialise le son d'ambiance du lecteur à 15%
    let audio = document.getElementById("generique");
    audio.volume = 0.15;

    // code ASCII
    let soundKeys = ["81","87","69","82","84","89","85","73","79","65","83","68","70" ,"71" ,"72","74"]

    let audioElement = $(this).find('.audio')[0];

    //  Joue le son attribué au clic
    $('.container').on('click','.wrapper_conga', function(){
     let audioElement = $(this).find('.audio')[0];
     audioElement.play();
    })

    for(let i = 0; i < soundKeys.length; i++) {
        $(document).keydown(function(e){

            let key = (e.keyCode ? e.keyCode : e.which);

            if(key == soundKeys[i]) {
                $('.audio')[i].play();
                //$('.wrapper_conga')[i].css('background-color', 'orange')
            }
        })
    }
});